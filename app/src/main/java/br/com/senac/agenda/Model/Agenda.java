package br.com.senac.agenda.Model;

/**
 * Created by marcelo on 06/03/18.
 */

public class Agenda {


    private String Nome;
    private String Endereco;
    private String Telefone;
    private String Celular;
    private String Foto;


    public Agenda(String nome, String endereco, String telefone, String celular, String foto) {
        this.Nome = nome;
        this.Endereco = endereco;
        this.Telefone = telefone;
        this.Celular = celular;
        this.Foto = foto;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getEndereco() {
        return Endereco;
    }

    public void setEndereco(String endereco) {
        Endereco = endereco;
    }

    public String getTelefone() {
        return Telefone;
    }

    public void setTelefone(String telefone) {
        Telefone = telefone;
    }

    public String getCelular() {
        return Celular;
    }

    public void setCelular(String celular) {
        Celular = celular;
    }

    public String getFoto() {
        return Foto;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }

    @Override
    public String toString() {
        return "Agenda{" +
                "Nome='" + Nome + '\'' +
                ", Endereco='" + Endereco + '\'' +
                ", Telefone='" + Telefone + '\'' +
                ", Celular='" + Celular + '\'' +
                ", Foto='" + Foto + '\'' +
                '}';
    }
}



