package br.com.senac.agenda.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.agenda.Model.Agenda;
import br.com.senac.agenda.R;

public class MainActivity extends AppCompatActivity {


    private ListView ListViewAgenda;
    private ArrayAdapter<Agenda> adapter;
    private Agenda contatoSelecionado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListViewAgenda = findViewById(R.id.listviewAgenda);

        Agenda contato1 = new Agenda("Joao da Silva","Rua Sete, n°50,Vila Velha - ES", "2222 2222","99999 9999","foto");
        Agenda contato2 = new Agenda("Franciso Santos","Rua oito, n°60,Vitoria - ES", "3333 3333","99999 9999","foto");
        Agenda contato3 = new Agenda("Maria da Silva","Rua nove, n°70,Serra - ES","4444 4444","99999 9999","foto");


        List<Agenda> Lista = new ArrayList<>();

        Lista.add(contato1);
        Lista.add(contato2);
        Lista.add(contato3);

        adapter = new ArrayAdapter<Agenda>(this,android.R.layout.simple_list_item_1,Lista);

        ListViewAgenda.setAdapter(adapter);

        ListViewAgenda.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int posicao, long indice) {

                contatoSelecionado = (Agenda)adapter.getItem(posicao);

                return false;
            }



        });




    }


}